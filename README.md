#### Exercício de Programação 3 - Orientação a Objetos 
#### Alunos: Ateldy Borges e Letícia de Souza
#### Matrículas: 15/0006101 e 15/0015160
#### Professor: Renato Coral Sampaio 

##### Definição do trabalho 
- Tema: Sistema interno da livraria Café com Poesia, desenvolvido em Ruby on Rails.
	- OBJETIVOS: Criar um sistema simples contendo CRUD’s com o intuito de facilitar o funcionamento de uma livraria.
	- FUNCIONALIDADES: O sistema é capaz de cadastrar um funcionário administrador da livraria, de modo que o mesmo, estando logado, possa realizar o cadastro dos demais funcionários, além de cadastrar livros. Os funcionários padrão poderão consultar e vender os livros desde que tenham sido devidamente cadastrados no sistema pelo administrador. 

#### Como usar
É necessário ter ambiente Ruby on Rails no Linux instalado e configurado e então seguir os seguintes comandos:
```sh
$ git clone https://gitlab.com/leticiadesouza/EP3OO_2016.git
```

```sh
$ cd EP3OO_2016/
$ rake db:migrate
$ rails s
```
Em seguida, abra seu navegador e digite o seguinte endereço:
* http://localhost:3000/users/new

#### Funcionalidades
- O MENU inicial apresenta o Login do administrador, que deverá ser feito previamente para o funcionamento das vendas que serão realizadas supostamente pelos funcionários que serão cadastrados. 
    - Na aba “Livros” é possível ver todos os livros cadastrados listados, além da opção “Novo Livro” para um administrador cadastrar livros, com título, autor, gênero e preço.
    - Na aba “Funcionários”, todos os funcionários cadastrados são listados e podem ser criados também a partir de um administrador logado. 
    - Na aba “Vendas” é listada as informações das vendas realizadas em ordem cronológicas. Apenas um funcionário já cadastrado pode realizar vendas, que são feitas a partir do título do livro e o preço. 
    
#### Classes Model	
- Foram criadas as classes funcionário, livro, user e venda. Na model de funcionário temos a validação do nome do funcionário. Na livro fazemos a validação para que ele possa ser buscado depois de criado. Na classe user temos a validação da senha. E na venda validamos que é necessário atribuir um valor numérico acima de 0.     
- A única opção disponível para quem não está logado e não é administrador, é a de busca, sendo assim, qualquer funcionário da livraria poderá consultar informações sobre os livros.