class Livro < ActiveRecord::Base

	def self.search(query)
		where("titulo LIKE :q", :q => "%#{query}%")
	end
end
 