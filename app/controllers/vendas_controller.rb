class VendasController < ApplicationController
  before_action :set_venda, only: [:show, :edit, :update, :destroy]

  # GET /vendas
  # GET /vendas.json
  def index
    @vendas = Venda.all
  end

  # GET /vendas/1
  # GET /vendas/1.json
  def show
  end

  # GET /vendas/new
  def new
    @venda = Venda.new
  end

  # GET /vendas/1/edit
  def edit
  end

  # POST /vendas
  # POST /vendas.json
  def create
     @funcionarios = Funcionario.all()
     @livros = Livro.all()
     @venda = Venda.new(venda_params)
     String nome_livro = venda_params[:livro]
    salvou =false
    String nome = venda_params[:funcionario]
    @funcionarios.each{|funcionario|
      print " \nFUNCIONARIO: " + funcionario.nome
      if (funcionario.nome.eql? nome)
        print "\nAchei ele: " + funcionario.nome + " e " + nome
        @livros.each{|livro|
          if (livro.titulo.eql? nome_livro)
            print "\n Achei o livro: " + livro.titulo + " e " + nome_livro
            @venda.save
            salvou = true
          end
        }
      end
    }
    respond_to do |format|
      if salvou
        format.html { redirect_to @venda, notice: 'Venda concluida com sucesso!'}
        format.json { render :show, status: :created, location: @venda }
      else
        format.html { redirect_to @venda, notice: 'Erro na venda, livro ou funcionario não encontrado' }
        format.json { render json: @venda.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /vendas/1
  # PATCH/PUT /vendas/1.json
  def update
    respond_to do |format|
      if @venda.update(venda_params)
        format.html { redirect_to @venda, notice: 'Venda was successfully updated.' }
        format.json { render :show, status: :ok, location: @venda }
      else
        format.html { render :edit }
        format.json { render json: @venda.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /vendas/1
  # DELETE /vendas/1.json
  def destroy
    @venda.destroy
    respond_to do |format|
      format.html { redirect_to vendas_url, notice: 'Venda was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_venda
      @venda = Venda.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def venda_params
      params.require(:venda).permit(:funcionario, :livro, :valor)
    end
  end
