class SugestoesController < ApplicationController
  before_action :set_sugesto, only: [:show, :edit, :update, :destroy]

  # GET /sugestoes
  # GET /sugestoes.json
  def index
    @sugestoes = Sugestoe.all
  end

  # GET /sugestoes/1
  # GET /sugestoes/1.json
  def show
  end

  # GET /sugestoes/new
  def new
    @sugesto = Sugestoe.new
  end

  # GET /sugestoes/1/edit
  def edit
  end

  # POST /sugestoes
  # POST /sugestoes.json
  def create
    @sugesto = Sugestoe.new(sugesto_params)

    respond_to do |format|
      if @sugesto.save
        format.html { redirect_to @sugesto, notice: 'Sugestoe was successfully created.' }
        format.json { render :show, status: :created, location: @sugesto }
      else
        format.html { render :new }
        format.json { render json: @sugesto.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sugestoes/1
  # PATCH/PUT /sugestoes/1.json
  def update
    respond_to do |format|
      if @sugesto.update(sugesto_params)
        format.html { redirect_to @sugesto, notice: 'Sugestoe was successfully updated.' }
        format.json { render :show, status: :ok, location: @sugesto }
      else
        format.html { render :edit }
        format.json { render json: @sugesto.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sugestoes/1
  # DELETE /sugestoes/1.json
  def destroy
    @sugesto.destroy
    respond_to do |format|
      format.html { redirect_to sugestoes_url, notice: 'Sugestoe was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sugesto
      @sugesto = Sugestoe.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sugesto_params
      params.require(:sugesto).permit(:nome, :email, :senha)
    end
end
