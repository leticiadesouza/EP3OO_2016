json.array!(@vendas) do |venda|
  json.extract! venda, :id, :funcionario, :livro, :valor
  json.url venda_url(venda, format: :json)
end
