require 'test_helper'

class SugestoesControllerTest < ActionController::TestCase
  setup do
    @sugesto = sugestoes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sugestoes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sugesto" do
    assert_difference('Sugestoe.count') do
      post :create, sugesto: { email: @sugesto.email, nome: @sugesto.nome, senha: @sugesto.senha }
    end

    assert_redirected_to sugesto_path(assigns(:sugesto))
  end

  test "should show sugesto" do
    get :show, id: @sugesto
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sugesto
    assert_response :success
  end

  test "should update sugesto" do
    patch :update, id: @sugesto, sugesto: { email: @sugesto.email, nome: @sugesto.nome, senha: @sugesto.senha }
    assert_redirected_to sugesto_path(assigns(:sugesto))
  end

  test "should destroy sugesto" do
    assert_difference('Sugestoe.count', -1) do
      delete :destroy, id: @sugesto
    end

    assert_redirected_to sugestoes_path
  end
end
