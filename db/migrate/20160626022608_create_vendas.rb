class CreateVendas < ActiveRecord::Migration
  def change
    create_table :vendas do |t|
      t.string :funcionario
      t.string :livro
      t.float :valor

      t.timestamps null: false
    end
  end
end
